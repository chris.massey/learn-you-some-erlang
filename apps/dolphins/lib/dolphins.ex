defmodule Dolphins do
  @doc """
  Creates a receive loop for a dolphin. Valid messages are:
    - `:flip`, which the dolphin rejects with a message and loops, and
    - `:fish.`, which the dolphin accepts with a message and terminates.

  ## Examples

      iex> dolphin = spawn(Dolphins, :dolphin, [])
      iex> send dolphin, {self(), :flip}
      iex> receive do
      ...>   response -> IO.puts response
      ...> end
      "How about no?"
      :ok
      iex> send dolphin, {self(), :fish}
      iex> receive do
      ...>   response -> IO.puts response
      ...> end
      "So long and thanks for all the fish!"
      :ok
      iex> Process.alive? dolphin
      false

  """
  def dolphin do
    receive do
      {from, :flip} ->
        send from, "How about no?"
        dolphin()
      {from, :fish} ->
        send from, "So long and thanks for all the fish!"
      _ ->
        IO.puts "Heh, we're smarter than you humans."
        dolphin()
    end
  end
end
