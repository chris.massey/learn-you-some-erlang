defmodule Kitchen do
  @moduledoc """
  Documentation for Kitchen.
  """

  def start(foods) do
    spawn __MODULE__, :fridge, [foods]
  end

  @doc """
  Store and take food from the fridge.

  ## Examples

      iex> fridge = Kitchen.start [:baking_soda]
      iex> send fridge, {self(), {:store, :bacon}}
      iex> receive do
      ...>   {_from, response} -> IO.inspect response
      ...> end
      :ok
      iex> send fridge, {self(), {:take, :bacon}}
      iex> receive do
      ...>   {_from, response} -> IO.inspect response
      ...> end
      {:ok, :bacon}
      iex> send fridge, {self(), {:take, :turkey}}
      iex> receive do
      ...>   {_from, response} -> IO.inspect response
      ...> end
      :not_found

  """
  def fridge(foods) when is_list(foods) do
    receive do
      {from, {:store, food}} ->
        send from, {self(), :ok}
        fridge([food | foods])
      {from, {:take, food}} ->
        case Enum.member?(foods, food) do
          true ->
            send from, {self(), {:ok, food}}
            List.delete(foods, food)
            |> fridge
          false ->
            send from, {self(), :not_found}
            fridge(foods)
        end
      :terminate -> :ok
    after 3000 -> :timeout
    end
  end

  @doc """
  Store food in the fridge.

  ## Examples

      iex> fridge = Kitchen.start [:baking_soda]
      iex> Kitchen.store fridge, :bacon
      :ok

  """
  def store(fridge, food) do
    send fridge, {self(), {:store, food}}
    receive do
      {_, response} -> response
    end
  end

  @doc """
  Take food from the fridge.

  ## Examples

      iex> fridge = Kitchen.start [:baking_soda, :bacon]
      iex> Kitchen.take fridge, :bacon
      {:ok, :bacon}
      iex> Kitchen.take fridge, :turkey
      :not_found

  """
  def take(fridge, food) do
    send fridge, {self(), {:take, food}}
    receive do
      {_, response} -> response
    end
  end
end
