defmodule Kitty do
  use GenServer

  require Record
  Record.defrecord :kitty, name: "", color: "", description: ""

  # Client

  @doc """
  ## Examples

      iex> {:ok, pid} = Kitty.start_link([])
      iex> Process.sleep(10)
      iex> send pid, "Testing handle_info"
      "Testing handle_info"
      iex> cat = Kitty.order_cat pid, "Cat Stevens", :white, "not actually a cat"
      {:kitty, "Cat Stevens", :white, "not actually a cat"}
      iex> Kitty.return_cat pid, cat
      :ok
      iex> Kitty.order_cat pid, "Kitten Mittens", :black, "look at them little paws!"
      {:kitty, "Cat Stevens", :white, "not actually a cat"}
      iex> Kitty.order_cat pid, "Kitten Mittens", :black, "look at them little paws!"
      {:kitty, "Kitten Mittens", :black, "look at them little paws!"}
      iex> Kitty.return_cat pid, cat
      :ok
      iex> Kitty.close_shop pid
      :ok

  """
  def start_link(default) do
    GenServer.start_link(__MODULE__, default)
  end

  def order_cat(pid, name, color, description) do
    GenServer.call(pid, {:order, name, color, description})
  end

  def return_cat(pid, cat) when Record.is_record(cat, :kitty) do
    GenServer.cast(pid, {:return, cat})
  end

  def close_shop(pid) do
    GenServer.call(pid, :terminate)
  end

  # Server (callbacks)

  def init(args) do
    {:ok, args}
  end

  def handle_call({:order, name, color, description}, _from, cats) do
    if cats == [] do
      {:reply, make_cat(name, color, description), cats}
    else
      {:reply, hd(cats), tl(cats)}
    end
  end

  def handle_call(:terminate, _from, cats) do
    {:stop, :normal, :ok, cats}
  end

  def handle_cast({:return, cat}, cats) when Record.is_record(cat, :kitty) do
    {:noreply, [cat | cats]}
  end

  def handle_info(_message, cats) do
    # IO.inspect message, label: "Kitty unexpected_message"
    {:noreply, cats}
  end

  def terminate(:normal, _cats) do
    # Enum.each cats, fn cat -> IO.puts "Kitty #{kitty(cat, :name)} was set free" end
    :ok
  end

  def code_change(_prev, cats, _extra) do
    {:ok, cats}
  end

  defp make_cat(name, color, description) do
    kitty(name: name, color: color, description: description)
  end
end
