defmodule Linkmon do
  @moduledoc """
  A band and album `critic` process with a `critic_supervisor` to restart the critic if the critic exits abnormally.
  """

  @doc """
  Start a critic.

  ## Examples

      iex> Linkmon.start
      iex> Process.sleep(10) # wait for processes to start
      iex> Linkmon.judge("The Doors", "Light my Firewall")
      "They are terrible!"
      iex> Process.whereis(:critic) |> Process.exit(:kill)
      true
      iex> Process.sleep(10) # wait for process to restart
      iex> Linkmon.judge("Rage Against the Turing Machine", "Unit Testify")
      "They are great!"

  """
  def start do
    spawn(__MODULE__, :critic_supervisor, [])
  end

  def judge(band, album) do
    ref = make_ref()
    send :critic, {self(), ref, {band, album}}

    receive do
      {_ref, criticism} -> criticism
    after 2000 -> :timeout
    end
  end

  def critic do
    receive do
      {from, ref, {"Rage Against the Turing Machine", "Unit Testify"}} ->
        send from, {ref, "They are great!"}
      {from, ref, {"System of a Downtime", "Memoize"}} ->
        send from, {ref, "They're not Johnny Crash but they're good."}
      {from, ref, {"Johnny Crash", "The Token Ring of Fire"}} ->
        send from, {ref, "Simply incredible."}
      {from, ref, {_band, _album}} ->
        send from, {ref, "They are terrible!"}
      catchall -> IO.inspect catchall, label: "critic()"
    end

    critic()
  end

  def critic_supervisor do
    Process.flag :trap_exit, true

    spawn_link(__MODULE__, :critic, [])
    |> Process.register(:critic)

    receive do
      {:EXIT, _pid, :normal} ->
        IO.puts "critic_supervisor() exit normal"
        :ok # not a crash
      {:EXIT, _pid, :shutdown} ->
        IO.puts "critic_supervisor() exit shutdown"
        :ok # manual termination, not a crash
      {:EXIT, _pid, reason} ->
        IO.puts "critic_supervisor() exit #{reason}"
        critic_supervisor()
      catchall -> IO.inspect catchall, label: "critic_supervisor()"
    end
  end
end
