defmodule Reminders.Event do
  require Record
  Record.defrecord :state, server: nil, name: "", delay: 0

  @doc """
  Start an event process until the event times out or is cancelled.

  ## Examples

      iex> Reminders.Event.start("Test", 0)
      iex> receive do
      ...>   {:done, "Test"} -> :ok
      ...> end
      :ok

      iex> pid = Reminders.Event.start("Test", 500)
      iex> Reminders.Event.cancel(pid)
      :ok

  """
  def start(name, delay) do
    spawn(__MODULE__, :init, [self(), name, delay])
  end

  def start_link(name, delay) do
    spawn_link(__MODULE__, :init, [self(), name, delay])
  end

  def init(server, name, delay) do
    loop(state(server: server, name: name, delay: normalize(delay)))
  end

  def cancel(pid) do
    ref = Process.monitor(pid)
    send pid, {self(), ref, :cancel}

    receive do
      {ref, :ok} ->
        Process.demonitor(ref, [:flush])
        :ok
      {:DOWN, ^ref, :process, ^pid, _reason} ->
        :ok
    end
  end

  @doc """
  Loop until the event times out or is cancelled.

  ## Examples

      iex> spawn(Reminders.Event, :loop, [Reminders.Event.state(server: self(), name: "Test", delay: [1])])
      iex> receive do
      ...>   {:done, "Test"} -> :ok
      ...> end
      :ok

      iex> pid = spawn(Reminders.Event, :loop, [Reminders.Event.state(server: self(), name: "Test", delay: [10])])
      iex> ref = make_ref()
      iex> send pid, {self(), ref, :cancel}
      iex> receive do
      ...>   {_ref, :ok} -> :ok
      ...> end
      :ok

  """
  def loop(state(server: server, name: name, delay: [delay | next_delays])) do
    receive do
      {server, ref, :cancel} ->
        send server, {ref, :ok}
    after delay * 1000 ->
      cond do
        next_delays == [] ->
          send server, {:done, name}
        next_delays != [] ->
          loop(state(server: server, name: name, delay: next_delays))
      end
    end
  end

  @doc """
  Normalize a delay given as an ISO 8601 datetime string.

  ## Examples

      iex> Reminders.Event.normalize("2000-01-01T00:00:00Z")
      [0]

  """
  def normalize(delay) when is_binary(delay) do
    now = DateTime.utc_now
    {:ok, target, _offset} = DateTime.from_iso8601(delay)

    diff = DateTime.diff(target, now)
    if diff > 0 do
      chunk_delay(diff)
    else
      [0]
    end
  end

  def normalize(delay) when is_number(delay) do
    chunk_delay(delay)
  end

  @doc """
  Chunk a delay larger than the BEAM timer max into multiple timers.

  ## Examples

      iex> Reminders.Event.chunk_delay(0)
      [0]

      iex> Reminders.Event.chunk_delay(1)
      [1]

      iex> Reminders.Event.chunk_delay(49 * 24 * 60 * 60)
      [4_233_600]

      iex> Reminders.Event.chunk_delay(49 * 24 * 60 * 60 + 1)
      [1, 4_233_600]

      iex> Reminders.Event.chunk_delay(2 * 49 * 24 * 60 * 60 + 2)
      [2, 4_233_600, 4_233_600]

  """
  def chunk_delay(delay) do
    limit = 49 * 24 * 60 * 60

    case remainder = rem(delay, limit) do
      0 when delay == 0 -> [0]
      0 -> List.duplicate(limit, div(delay, limit))
      _ -> [remainder | List.duplicate(limit, div(delay, limit))]
    end
  end
end
