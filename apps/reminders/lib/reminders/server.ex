defmodule Reminders.Server do
  require Record
  Record.defrecord :state, events: %{}, clients: %{}
  Record.defrecord :event, name: "", description: "", pid: nil, timeout: DateTime.from_iso8601("1970-01-01T00:00:00Z")

  def child_spec(_opts) do
    %{
      id: __MODULE__,
      start: {__MODULE__, :start_link, []},
      type: :worker,
      restart: :permanent,
      shutdown: 500
    }
  end

  @doc """
  ## Examples

      iex> Reminders.Server.start()
      iex> Process.sleep(10) # wait for processes to start
      iex> {:ok, _} = Reminders.Server.subscribe self()
      iex> Process.sleep(10) # wait for processes to start
      iex> Reminders.Server.add_event("Hey there", "test", 1)
      :ok
      iex> Reminders.Server.listen(2)
      [{:done, "Hey there", "test"}]

  """
  def start do
    pid = spawn(__MODULE__, :init, [])
    Process.register(pid, __MODULE__)
    pid
  end

  def start_link do
    pid = spawn_link(__MODULE__, :init, [])
    Process.register(pid, __MODULE__)
    {:ok, pid}
  end

  def init do
    loop(state())
  end

  def terminate do
    send __MODULE__, :shutdown
  end

  @doc """
  """
  def subscribe(pid) do
    ref = Process.whereis(__MODULE__) |> Process.monitor
    send __MODULE__, {self(), ref, {:subscribe, pid}}

    receive do
      {ref, :ok} -> {:ok, ref}
      {:DOWN, _ref, :process, _pid, reason} -> {:error, reason}
    after 5000 -> {:error, :timeout}
    end
  end

  @doc """
  """
  def add_event(name, description, timeout) do
    ref = make_ref()
    send __MODULE__, {self(), ref, {:add, name, description, timeout}}

    receive do
      {_ref, message} -> message
    after 5000 -> {:error, :timeout}
    end
  end

  @doc """
  """
  def cancel(name) do
    ref = make_ref()
    send __MODULE__, {self(), ref, {:cancel, name}}

    receive do
      {_ref, :ok} -> :ok
    after 5000 -> {:error, :timeout}
    end
  end

  def listen(delay) do
    receive do
      message = {:done, _name, _description} ->
        [message | listen(0)]
      unknown ->
        [unknown]
    after delay * 1000 ->
      []
    end
  end

  def loop(state(events: events, clients: clients)) do
    receive do
      {pid, ref, {:subscribe, client}} ->
        monitor_ref = Process.monitor(pid)
        new_clients = Map.put(clients, monitor_ref, client)
        send pid, {ref, :ok}
        loop(state(events: events, clients: new_clients))
      {pid, ref, {:add, name, description, timeout}} ->
        case validate_datetime(timeout) do
          true ->
            event_pid = Reminders.Event.start_link(name, timeout)
            new_events = Map.put(events, name, event(name: name, description: description, pid: event_pid, timeout: timeout))
            send pid, {ref, :ok}
            loop(state(events: new_events, clients: clients))
          false ->
            send pid, {ref, {:error, :invalid_format}}
            loop(state(events: events, clients: clients))
        end
      {pid, ref, {:cancel, name}} ->
        {event, _} = Map.pop(events, name)
        if event != nil do
          Reminders.Event.cancel(event.pid)
        end
        send pid, {ref, :ok}
        loop(state(events: events, clients: clients))
      {:done, name} ->
        {event, _} = Map.pop(events, name)
        if event != nil do
          send_to_clients(clients, {:done, event(event, :name), event(event, :description)})
        end
        loop(state(events: events, clients: clients))
      {:DOWN, ref, :process, _pid, _reason} ->
        Map.pop(clients, ref)
        loop(state(events: events, clients: clients))
      :code_change ->
        __MODULE__.loop(state(events: events, clients: clients))
      :shutdown ->
        exit(:shutdown)
      unknown ->
        IO.inspect unknown, label: "Reminders.Event.loop() unknown message"
    end
  end

  def send_to_clients(clients, message) do
    Enum.each clients, fn {_ref, pid} -> send(pid, message) end
  end

  @doc """
  Validates a datetime string.

  ## Notes
    - Use [Timex](https://github.com/bitwalker/timex), e.g. `[{:timex, "~> 3.1"}]`,
    - Consider [Vex](https://github.com/CargoSense/vex),
    - Consider [Domain Specific Languages](https://elixir-lang.org/getting-started/meta/domain-specific-languages.html) advice.

  """
  def validate_datetime(datetime) when is_binary(datetime) do
    case DateTime.from_iso8601(datetime) do
      {:ok, _, _} -> true
      {:error, _} -> false
    end
  end

  def validate_datetime(datetime) when is_number(datetime) do
    true
  end
end
