defmodule Mix.Tasks.Road.Shortest do
  use Mix.Task

  @shortdoc "Calculate the shortest route between two tracks."

  @moduledoc """
  A task for calculating the shortest route between two tracks.

  To use, run `mix road.shortest [filename]`.

  References:
    - [Create a Mix Task for an Elixir Project](http://joeyates.info/2015/07/25/create-a-mix-task-for-an-elixir-project/)
  """

  def run(filename) do
    Road.main(filename)
    |> IO.inspect
  end
end
