defmodule Road do
  @moduledoc """
  An algorithm to find the shortest route between two tracks, A and B,
  with crossovers X between every section.
  """

  def open(filename) do
    File.read!(filename)
    |> calculate
  end

  @doc """
  Find the shortest route using two paths with crossovers.

  ## Examples

      iex> Road.calculate("10 11 0")
      [{:a, 10}]
      iex> Road.calculate("10 9 0")
      [{:b, 9}]
      iex> Road.calculate("10 15 2 7 3 0")
      [{:a, 10}, {:x, 2}, {:b, 3}]

  """
  def calculate(text) do
    String.trim(text)
    |> String.split("\s")
    |> Enum.map(&String.to_integer/1)
    |> group_values([])
    |> optimal_path
  end

  def group_values([], groups), do: Enum.reverse(groups)
  def group_values([a, b, x | rest], groups) do
    group_values(rest, [{a, b, x} | groups])
  end

  def shortest_step({a, b, x}, {{a_length, a_path}, {b_length, b_path}}) do
    a1 = {a_length + a, [{:a, a} | a_path]}
    a2 = {a_length + b + x, [{:x, x}, {:b, b} | b_path]}
    b1 = {b_length + b, [{:b, b} | b_path]}
    b2 = {b_length + a + x, [{:x, x}, {:a, a} | a_path]}
    {min(a1, a2), min(b1, b2)}
  end

  def optimal_path(groups) do
    {a, b} = List.foldl groups, {{0, []}, {0, []}}, &shortest_step/2
    {_, path} =
      cond do
        List.first(elem(a, 1)) != {:x, 0} -> a
        List.first(elem(b, 1)) != {:x, 0} -> b
      end

    Enum.reverse(path)
  end
end
