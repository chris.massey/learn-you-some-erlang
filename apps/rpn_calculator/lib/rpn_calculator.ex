defmodule Calculator do
  @moduledoc """
  A Reverse Polish Notation (RPN) calculator for Elixir.

  Supported operations are:
    - "+" addition
    - "-" subtraction
    - "*" multiplication
    - "/" division
    - etc
  """

  @doc """
  Calculate an RPN expression.

  ## Examples

      iex> Calculator.rpn("12 3 +")
      15.0
      iex> Calculator.rpn("12 3 -")
      9.0
      iex> Calculator.rpn("12 3 *")
      36.0
      iex> Calculator.rpn("12 3 /")
      4.0
      iex> Calculator.rpn("4 2 ^")
      16.0
      iex> Calculator.rpn("4 log2")
      2.0
      iex> Calculator.rpn("100 log")
      2.0
      iex> Calculator.rpn("1 2 3 4 sum")
      10.0
      iex> Calculator.rpn("1 2 3 4 prod")
      24.0
      iex> Calculator.rpn("1 2 ^ 2 2 ^ 3 2 ^ 4 2 ^ sum 2 -")
      28.0

  """
  def rpn(expression) when is_binary(expression) do
    String.split(expression)
    |> List.foldl([], &rpn/2)
    |> List.first
  end

  defp rpn("+", [a, b | stack]) when is_number(a) and is_number(b), do: [b + a | stack]
  defp rpn("-", [a, b | stack]) when is_number(a) and is_number(b), do: [b - a | stack]
  defp rpn("*", [a, b | stack]) when is_number(a) and is_number(b), do: [b * a | stack]
  defp rpn("/", [a, b | stack]) when is_number(a) and is_number(b), do: [b / a | stack]
  defp rpn("^", [a, b | stack]) when is_number(a) and is_number(b), do: [:math.pow(b, a) | stack]
  defp rpn("log2", [a | stack]) when is_number(a), do: [:math.log2(a) | stack]
  defp rpn("log", [a | stack]) when is_number(a), do: [:math.log10(a) | stack]

  defp rpn("sum", stack) when is_list(stack) do
    # List.foldl(stack, 0.0, fn number, sum -> sum + number end)
    Enum.sum(stack)
    |> List.wrap
  end

  defp rpn("prod", stack) when is_list(stack) do
    # List.foldl(stack, 1.0, fn number, prod -> prod * number end)
    List.foldl(stack, 1.0, &:erlang.'*'/2)
    |> List.wrap
  end

  defp rpn(token, stack) when is_binary(token) and is_list(stack) do
    [read(token) | stack]
  end

  @doc """
  Parses a string token into a float.

  ## Examples

      iex> Calculator.read("1")
      1.0
      iex> Calculator.read("1.0")
      1.0
      iex> Calculator.read("a")
      :error

  """
  def read(token) do
    case Float.parse(token) do
      {result, _} -> result
      :error -> :error
    end
  end
end
